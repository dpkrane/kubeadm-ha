terraform {
  required_providers {
    proxmox = {
      source  = "telmate/proxmox"
      version = "2.9.14"
    }
  }
}

provider "proxmox" {
  pm_api_url          = "https://proxmoxxx:8006/api2/json"
  pm_api_token_id     = "terraform@pve!mytoken"
  pm_api_token_secret = "96c6ada9-3969-4ea0-b0a8-a2666e06a9b0"
  pm_tls_insecure     = true
}

resource "proxmox_vm_qemu" "kube_master" {
  count       = 3
  name        = "kube-master-0${count.index + 1}"
  vmid        = "40${count.index + 1}"
  target_node = var.proxmox_host
  clone       = var.template_name
  agent       = 1
  onboot      = true
  os_type     = "cloud-init"
  qemu_os     = "other"
  ciuser      = "dpkrane"
  cores       = 2
  sockets     = 1
  cpu         = "host"
  memory      = 2048
  scsihw      = "virtio-scsi-pci"
  bootdisk    = "scsi0"
  disk {
    slot     = 0
    size     = "36G"
    type     = "scsi"
    storage  = "local-lvm"
    iothread = 0
  }

  network {
    model  = "virtio"
    bridge = "vmbr0"
  }

  network {
    model  = "virtio"
    bridge = "vmbr17"
  }

  lifecycle {
    ignore_changes = [
      network,
    ]
  }

  ipconfig0 = "ip=192.168.1.24${count.index + 1}/24,gw=192.168.1.1"
  ipconfig1 = "ip=10.17.0.1${count.index + 1}/24"

  sshkeys = <<EOF
  ${var.ssh_key}
  EOF
}

resource "proxmox_vm_qemu" "kube_node" {
  count       = 3
  name        = "kube-node-0${count.index + 1}"
  vmid        = "50${count.index + 1}"
  target_node = var.proxmox_host
  clone       = var.template_name
  agent       = 1
  onboot      = true
  os_type     = "cloud-init"
  qemu_os     = "other"
  ciuser      = "dpkrane"
  cores       = 2
  sockets     = 1
  cpu         = "host"
  memory      = 4096
  scsihw      = "virtio-scsi-pci"
  bootdisk    = "scsi0"

  disk {
    slot     = 0
    size     = "36G"
    type     = "scsi"
    storage  = "local-lvm"
    iothread = 0
  }

  network {
    model  = "virtio"
    bridge = "vmbr0"
  }

  network {
    model  = "virtio"
    bridge = "vmbr17"
  }

  lifecycle {
    ignore_changes = [
      network,
    ]
  }

  ipconfig0 = "ip=192.168.1.25${count.index + 1}/24,gw=192.168.1.1"
  ipconfig1 = "ip=10.17.0.2${count.index + 1}/24"

  sshkeys = <<EOF
  ${var.ssh_key}
  EOF
}
